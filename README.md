# dCMTF 

dCMTF is a software package to perform [Multi-way Clustering of Augmented Multi-view Data by Deep Collective Matrix Tri-factorization](https://arxiv.org/abs/2009.05805). 

The sample data used in the documentation examples can be downloaded from [here](https://drive.google.com/file/d/1TZ7YUJ33VrVBb1R_nqUoKEVpSoqUzFvR/view?usp=sharing).

## dCMTF prerequisite 
- python 3.7.6
- pytorch 1.5.0
- numpy 1.18.1
- pandas 1.0.4
- scikit-learn 0.23.1
- scipy 1.4.1

## Running dCMTF 

Following are the 2 ways to run dCMTF

- **dcmtf** -  To run dCMTF for arbitrary collection of matrices and given hyperparameter setting. See `doc/dcmtf_documentation_and_example_usage.ipynb` for documentation and example.

- **dcmtf + Ax** - To run dCMTF for arbitrary collection of matrices and perform best hyperparameter search using Adaptive Experimentation Platform (Ax). The best hyperparamter setting can either be selected based on (lowest) training loss or (highest) unsupervised performance metric. See `doc/dcmtf_documentation_and_example_usage_with_ax.ipynb` for documentation and example.
